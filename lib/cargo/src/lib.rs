use std::os::raw::{c_char};
use std::ffi::{CString, CStr};
use std::env;

#[no_mangle]
pub extern fn rust_greeting(){
	let key = "RUST_LOG";
	env::set_var(key, "info");
	let rt  = tokio::runtime::Runtime::new().unwrap();
	rt.block_on(slowtransfer::client::client_main(&"/home/querens/Documents/RustProjetcs/slowtransfer/client.toml".to_string()));
}


#[cfg(target_os="android")]
#[allow(non_snake_case)]
pub mod android {
    extern crate jni;

    use super::*;
    use self::jni::JNIEnv;
    use self::jni::objects::{JClass, JString};
    use self::jni::sys::{jstring};

    #[no_mangle]
    pub unsafe extern fn Java_com_example_greetings_RustGreetings_greeting(env: JNIEnv, _: JClass) -> () {
	rust_greeting();
    }
}
